//
//  ContentView.swift
//  AutomationTest
//
//  Created by Vanitha on 2020-07-28.
//  Copyright © 2020 Vanitha. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
